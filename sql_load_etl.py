# this python script will do the loading and etl of the csv file and putting it into postgres
# i use psycopg2 as the library for sql
# i use pandas for reading the CSV file and transforming the data

import pandas as pd
import psycopg2

# first I'm creating a function that connects to the default postgres database and creates the new database
# since I'm using the postgres app on my Macbook Pro, the login info is the defaults for the app

def create_database():
    # connect to default database in postgres app
    # default username and db are taken from my macbook information, password is none
    conn = psycopg2.connect("host=127.0.0.1 dbname=Ryan user=Ryan password=")
    conn.set_session(autocommit=True)
    cur = conn.cursor()
    
    # create werkspot database with UTF8 encoding
    # using template0
    cur.execute("CREATE DATABASE werkspot WITH ENCODING 'utf8' TEMPLATE template0")

    # close connection to default database
    conn.close()    
    
    # connect to werkspot database
    conn = psycopg2.connect("host=127.0.0.1 dbname=werkspot user=Ryan password=")
    cur = conn.cursor()
    
    return cur, conn

# this function performs the etl on the file provided and returns multiple list to be used in the load
# first it reads the csv into a dataframe
# then tranforms the data, splitting it into multiple dataframes (one for each table)
# then those dataframes are turned into lists to load with SQL

def etl():
    # load file, reading in the csv which has ; as the separator
    # i could have had a variable file here and brought it in through the function when i call it 
    # instead of coding it in but i didnt think it was necessary
    df=pd.read_csv("/Users/Ryan/Desktop/Data_Engineering/werkspot job/event_log .csv", sep=';')
    
    #first we split the meta_data column into individual columns based on data dictionary
    df = df.join(df['meta_data'].str.split('_', expand=True).add_prefix('r'))
    df.rename(columns={'r0': 'service_id', 'r1': 'service_name_nl', \
        'r2': 'service_name_en', 'r3': 'lead_fee'}, inplace=True)
    #can i do this in one step? probably? i just don't know how
    
    #delete meta_data column? yeah i think so, i don't need it anymore
    df = df.drop('meta_data', 1)
    
    #just gonna change professional_id_anonymized just to professional_id
    df.rename(columns={'professional_id_anonymized': 'professional_id'}, inplace=True)
    
    #so far as i can tell there's no nans in the data that aren't the meta_data column
    #replace with 'NA' as text so it looks cleaner and SQL has an easier time
    #this means service_id and lead_fee will have to be TEXT type in the database to accept the value
    df[['service_id', 'service_name_nl', 'service_name_en', 'lead_fee']] \
            = df[['service_id', 'service_name_nl', 'service_name_en', 'lead_fee']].fillna('NA')
    
    #convert created_at column to datetime
    df['created_at'] = pd.to_datetime(df['created_at'], format='%Y%m%d %H:%M:%S')
    
    #add time columns to dataframe
    #can i do this in one line? i probably look like a bad programmer
    df['hour'] = df.created_at.dt.hour
    df['day'] = df.created_at.dt.day
    df['month'] = df.created_at.dt.month 
    df['year'] = df.created_at.dt.year
    
    #etl is done, I don't know if I did enough but it goes into the tables the way I set them up
    #now i create the files which will be returned and used to insert into the database tables
    #events table (fact table)
    events_table = df[['event_id', 'event_type', 'professional_id']].values.tolist()
    
    #event dates (dimension)
    event_dates = df[['event_id', 'created_at', 'hour', 'day', 'month', 'year']].values.tolist()
    
    #professionals (dimension)
    professionals = df[['professional_id', 'service_id']]
    #need to remove all the NA rows since this isn't needed for a lookup
    professionals = professionals[professionals.service_id != 'NA']
    #now take the values out for the SQL line
    professionals = professionals[['professional_id', 'service_id']].values.tolist()
    
    #last is the services table (dimension)
    services = df[['service_id', 'service_name_nl', 'service_name_en', 'lead_fee']]
    #need to remove all the NA rows since this isn't needed for a lookup
    services = services[services.service_id != 'NA']
    #now take the values out for the SQL line
    services = services[['service_id', 'service_name_nl', 'service_name_en', 'lead_fee']].values.tolist()
    
    return events_table, event_dates, professionals, services

# this function is to create the dimension and fact tables in the werkspot database that will then be filled later
# theres 4 tables I'm making: events, event_dates, professionals, and services
# see the readme file for the logic/nonsense of why I did it this way

def create_tables(cur, conn):
    #schema information can be found in the readme file for this project    
        
    #create the events table if it doesn't exist
    #event_id is unique so it's the primary key
    event_table_create = ("CREATE TABLE IF NOT EXISTS events (event_id INT PRIMARY KEY UNIQUE, \
                                event_type TEXT, professional_id INT)")
    cur.execute(event_table_create)
        
        
    event_dates_table_create = ("CREATE TABLE IF NOT EXISTS event_dates \
                                (event_id INT PRIMARY KEY UNIQUE, \
                                created_at TEXT, hour INT, day INT, month INT, year INT)")
    cur.execute(event_dates_table_create)
        
        
    professionals_table_create = ("CREATE TABLE IF NOT EXISTS professionals (professional_id INT, \
                                service_id INT)")
    cur.execute(professionals_table_create)
        
        
    services_table_create = ("CREATE TABLE IF NOT EXISTS services (service_id TEXT PRIMARY KEY, \
                                service_name_nl TEXT, service_name_en TEXT, lead_fee TEXT)")
    cur.execute(services_table_create)
        
    conn.commit()


# this function takes the lists from the etl function and puts the data into postgres
# each list loads into its respective table
def load_tables(cur, conn, events_table, event_dates, professionals, services):

    # each of the dataframes is output as a list, so i need to take the values out and pull those in 
    # to cur.execute
    # cur.execute only takes like a list of tuples or something similar
    for i, val in enumerate(events_table):
        events_table_insert = ("INSERT INTO events (event_id, event_type, professional_id) \
                            VALUES (%s, %s, %s) ON CONFLICT (event_id) DO NOTHING;")
        cur.execute(events_table_insert, val)
        
    for i, val in enumerate(event_dates):
        event_dates_table_insert = ("INSERT INTO event_dates \
                                (event_id, created_at, hour, day, month, year) \
                                VALUES (%s, %s, %s, %s, %s, %s) ON CONFLICT (event_id) DO NOTHING;")
        cur.execute(event_dates_table_insert, val)

    for i, val in enumerate(professionals):
        professionals_table_insert = ("INSERT INTO professionals (professional_id, service_id) \
                                VALUES (%s, %s);")
        cur.execute(professionals_table_insert, val)
        
    for i, val in enumerate(services):
        services_table_insert = ("INSERT INTO services \ 
                                (service_id, service_name_nl, service_name_en, lead_fee) \
                                VALUES (%s, %s, %s, %s) ON CONFLICT (service_id) DO NOTHING;")
        cur.execute(services_table_insert, val)
    
    conn.commit()

#this is the function that creates the availability snapshot table
def snapshot_create(curr, con):
    snapshot = ("""CREATE TABLE IF NOT EXISTS availability_snapshot AS (
            WITH active AS ( 
            ((SELECT date, 
            sum(act_cnt) over (order by date) as active_count
            FROM
            (SELECT date(ed.created_at) as date, count(distinct e.professional_id) as act_cnt
            FROM events as e
            JOIN event_dates as ed
            ON e.event_id = ed.event_id
            WHERE event_type = 'became_able_to_propose'
            GROUP BY date(ed.created_at)
            HAVING date(ed.created_at) BETWEEN min(date(ed.created_at)) AND '2020-03-10') AS x))),
            inactive AS (((SELECT date, 
            sum(in_cnt) over (order by date) as inactive_count
            FROM
            (SELECT date(ed.created_at), count(distinct e.professional_id) as in_cnt
            FROM events as e
            JOIN event_dates as ed
            ON e.event_id = ed.event_id
            WHERE event_type = 'became_unable_to_propose'
            GROUP BY date(ed.created_at)
            HAVING date(ed.created_at) BETWEEN min(date(ed.created_at)) AND '2020-03-10') AS y))) 
            
            SELECT 
                 active.date,
                 CASE
                     WHEN active_count IS NULL THEN (lag(active_count, 1) 
                        over (order by active.date) - inactive_count)
                     WHEN inactive_count IS NULL THEN (active_count - 0)
                     ELSE (active_count - inactive_count)
                 END active_professionals_count
            FROM active
            FULL OUTER JOIN inactive
            ON active.date = inactive.date
            ORDER BY date 
            );""")
            
    cur.execute(snapshot)
    conn.commit()

# this is the main program that runs the functions
def main():
    # create database and connect
    cur, conn = create_database()
    
    # create tables
    create_tables(cur, conn)
    #do the etl and store to the lists
    events_table, event_dates, professionals, services = etl()
    #pass the lists and load the tables into sql
    load_tables(cur, conn, events_table, event_dates, professionals, services)
    #create the availability snapshot
    snapshot_create(curr, con)

    #close connection
    conn.close()

# i need this to run python programs in case the name is different on another system
# or this is run inside something else which affects the name
if __name__ == "__main__":
    main()

