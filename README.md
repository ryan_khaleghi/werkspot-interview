# werkspot-interview

This project is part of my interview process at Werkspot. To be deleted once interview is complete.

In this project I have two tasks: build an ETL pipeline into a database I create for data in a provided CSV file, and then a table that is a snapshot of certain data.

For the ETL pipeline, I am using the Python library psycopg2 to connect to the Postgres SQL database for the load, and pandas dataframes to extract and transform it. 

I decided not to stage the data first before transform, because it's one simple CSV file. I'm assuming I would have pulled it from the data warehouse, so the data already exists there. I found a nice github project on using dbt to do this, but I'm not familiar with it, so I'm using my data science pipeline skills.

I modeled the database in a fact/dimension model from the data given. Since the "event" being recorded is the professional having a change status event (create, became able to propose, etc), I set that as the fact table. 

The fact table "events" has 3 attributes: event_id (unique, primary key), event_type, and professional_id. I thought this was the basic business process being tracked and entered, and would not have any duplicate rows or extraneous detail.

For the dimension tables "event_dates", "professionals" and "services", I give the who/what/where/when/why detail for the fact. event_dates has the dates and times of each recorded status change event: event_id, created_at timestamp, and hour, day, month, and year extracted from that timestamp for reporting/analysis. "professionals" is just the professional_id and service_ids that are offered. services is an index of the service_id, service_name_nl, service_name_en, and lead_fee. Since these fields only exist for one event_type, it would clutter the other tables unnecessarily to have duplicate rows for each service. This creates a problem where most queries require multiple joins, which isn't ideal. I wonder if I sliced too finely an already small dataset, but I wanted to keep the facts clean and allow redundancy elsewhere - but not too much. 

Finally, included is the availability snapshot for question 2. This just pulls data from the existing tables, calculates the running total of activations and inactivations, and outputs it for a given day.

Some notes:
- This took me longer than expected, so I finished the SQL stuff but didn't get to the Docker portion. I'm not good at Docker, especially when it has to do something like startup a postgres server, so this would take me some time. 
- - I'm also not very familiar with version control from my computer (like pushing updates to gitlab), so I used the online editor to code, commit changes and make comments on my commits. I tested on my own computer.
- I did some research about this while working on it, as well, and realized there's a lot of stuff I don't know/didn't include that probably would need to be on the job, such as unit testing, error handling, and maybe optimizing the queries. I consider myself intermediate at SQL, since I can do CTEs and window functions, but optimization is something I consider advanced and don't really know in practice. As for unit testing and error handling, I've never done much of that either.
- I still have a lot to learn, and I'm rusty after not being employed for a while.